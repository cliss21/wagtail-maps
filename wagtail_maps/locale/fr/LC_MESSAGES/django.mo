��    !      $  /   ,      �     �       *        @     \  N   u     �     �     �  	   �     �     �       0        K     Q     X  �   h  W   �     C     [     t     |     �     �  	   �     �     �     �     �     �     �    �     �        2     #   F     j  U   �     �     �     �  	   �          	        ?   7     w     }     �  �   �  ]   ,	     �	     �	     �	     �	     �	     �	  	   �	     �	     
     
     
     
     
                                                                          
                           	                                                   !       Calculate from the points Center of the map Choose between a link to a page or an URL. Coordinates from geo URI… Coordinates of the point Enter the geo URI of your point to set its latitude and longitude accordingly. Height (px) Initial zoom Latitude Longitude Map Maximum zoom level Minimum zoom level Minimum zoom level must be smaller than maximum. Point Points Set coordinates Unable to calculate the geometric center of the points. Verify that you have at least one point with valid latitude and longitude. Unable to parse the geo URI. Verify that it is in the form of <code>geo:1.0,2.0</code>. center point's latitude center point's longitude content latitude link to a page link to an URL longitude map maps name point points title Project-Id-Version: wagtail-maps
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-06-07 11:42+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Calculer à partir des points Centre de la carte Choisissez entre un lien vers une page ou une URL. Coordonnées depuis une URI géo… Coordonnées du point Entrez l'URI géo du point pour définir sa latitude et sa longitude en conséquence. Hauteur (px) Zoom initial Latitude Longitude Carte Niveau de zoom maximum Niveau de zoom minimum Le niveau de zoom minimum doit être plus petit que le maximum. Point Points Définir les coordonnées Impossible de calculer le centre géographique des points. Vérifiez qu'au moins un point est défini avec une latitude et longitude valides. Impossible d'analyser l'URI géo. Vérifiez qu'elle est de la forme <code>geo:1.0,2.0</code>. latitude du point central longitude du point central Contenu latitude lien vers une page lien vers une URL longitude carte cartes nom point points Titre 