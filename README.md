# wagtail-maps

Create and display maps with points in Wagtail.

**Warning!** This project is still early on in its development lifecycle. It is
possible for breaking changes to occur between versions until reaching a stable
1.0. Feedback and pull requests are welcome.

This package extend Wagtail to add a new Map model, which is composed by one or
more points. Each point may have a title, some content and link to an internal
or external URL. Once you have configured your map from the Wagtail admin, you
will be able to display it in a page - e.g. as a StreamField block.

## Requirements

This package requires the following:
- **Wagtail** (4.1 LTS, 5.0)
- **Django** (3.2 LTS, 4.2 LTS)
- **Python 3** (3.8, 3.9, 3.10, 3.11)

## Installation

1. Install using ``pip``:
   ```shell
   pip install wagtail-maps
   ```
2. Add ``wagtail_maps`` to your ``INSTALLED_APPS`` setting:
   ```python
   INSTALLED_APPS = [
       # ...
       'wagtail_maps',
       # ...
   ]
   ```
3. Include the URL of *wagtail-maps* to your ``urls.py`` file:
   ```python
   from wagtail_maps import urls as wagtailmaps_urls

   urlpatterns = [
       # ...
       path('maps/', include(wagtailmaps_urls)),
       # ...
   ]
   ```
4. Run ``python manage.py migrate`` to create the models

## Usage

A StreamField block `wagtail_maps.blocks.MapBlock` can be used to display a
given `Map` object with its points in your page. To render this block in your
frontend, you will have to include the following assets in the templates which
can contain it - or in your base template:

* the JavaScript code that renders the map with [Leaflet] thanks to a
  [Stimulus] controller:
  ```django
  <script src="{% static "wagtail_maps/js/map-block.js" %}"></script>
  ```
* the default Leaflet stylesheet if you don't provide your own:
  ```django
  <link rel="stylesheet" href="{% static "wagtail_maps/vendor/leaflet.css" %}">
  ```

[Leaflet]: https://leafletjs.com/
[Stimulus]: https://stimulus.hotwired.dev/

## Development
### Quick start

To set up a development environment, ensure that Python 3 is installed on your
system. Then:

1. Clone this repository
2. Create a virtual environment and activate it:
   ```shell
   python3 -m venv venv
   source venv/bin/activate
   ```
3. Install this package and its requirements:
   ```shell
   (venv)$ pip install -r requirements-dev.txt
   ```

### Contributing

The tests are written with [pytest] and code coverage is measured with [coverage].
You can use the following commands while developing:
- ``make test``: run the tests and output a quick report of code coverage
- ``make test-wip``: only run the tests marked as 'wip'
- ``make test-all``: run the tests on all supported versions of Django and
  Wagtail with [nox]

The Python code is formatted and linted thanks to [flake8], [isort] and [black].
All of these tools are configured in [pre-commit] and you should consider to
install its git hook scripts by running:
```shell
(venv)$ pre-commit install
```

When submitting a pull-request, please ensure that the code is well formatted
and covered, and that all the tests pass.

[pytest]: https://docs.pytest.org/
[coverage]: https://coverage.readthedocs.io/
[nox]: https://nox.thea.codes/
[flake8]: https://flake8.pycqa.org/
[isort]: https://pycqa.github.io/isort/
[black]: https://black.readthedocs.io/
[pre-commit]: https://pre-commit.com/

## License

This extension is mainly developed by [Cliss XXI](https://www.cliss21.com) and
licensed under the [AGPLv3+](LICENSE). Any contribution is welcome!
